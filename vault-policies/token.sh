#!/usr/bin/env bash
vault token create -orphan -policy="autounseal" \
   -period=24h > wrapping-token.txt

