# Bsides presentation classroom

## Setup

This repo will work against a RockyLinux 9 system.
It needs access to a DigitalOcean token for setting up the letsencrypt stuff.

* Create your wildcard domain
* Put that wildcard domain in `inv/group_vars/all/global.yml`
* Delete `inv/group_vars/all/secrets.yml`
* create your own ansible-vault password file in `.vault/password`
* put your DigitalOcean token in `inv/group_vars/all/secrets.yml` by using the 
  command
  ```
  ansible-vault create inv/group_vars/all/secrets.yml
  ```
* Edit inv/hosts.yml

# Running

```
ansible-playbook -i inv/ playbooks/classroom.yml
```